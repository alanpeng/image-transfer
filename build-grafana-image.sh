#!/bin/bash
GrafanaVersion=9.1.5

curl -L -o v${GrafanaVersion}.tar.gz https://github.com/grafana/grafana/archive/refs/tags/v${GrafanaVersion}.tar.gz
tar zxf v${GrafanaVersion}.tar.gz
git clone https://github.com/wangriyu/docker-grafana
cp docker-grafana/wechat-notifier/wechat*.go grafana-${GrafanaVersion}/pkg/services/alerting/notifiers/
cp -f Dockerfile grafana-${GrafanaVersion}
cp .npmrc grafana-${GrafanaVersion}
cd grafana-${GrafanaVersion}
make build-docker-full
